# spec/factories/tracks.rb
require 'faker'

FactoryGirl.define do
  factory :track do |f|
    f.data_point_id { rand(100000)}
    f.engine_coolant_temp {rand(100)}
    f.total_used_fuel {rand(100)}
    f.vehicle_speed {rand(100)}
  end
end