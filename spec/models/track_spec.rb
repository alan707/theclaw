# spec/models/track.rb
require 'spec_helper'

describe Track do 
  it "has a valid factory" do
    FactoryGirl.create(:track).should be_valid
  end

  it "is invalid without a data_point_id" do
    FactoryGirl.build(:track, :data_point_id => nil).should_not be_valid
  end

  it "is invalid without a unique data_point_id" do
    FactoryGirl.create(:track, :data_point_id => "23")
    FactoryGirl.build(:track, :data_point_id => "23").should_not be_valid
  end


  it "displays engine_coolant_temp"
  it "displays total_used_fuel"
  it "displays vehicle_speed"
end