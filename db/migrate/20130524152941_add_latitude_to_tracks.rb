class AddLatitudeToTracks < ActiveRecord::Migration
  def change
    add_column :tracks, :latitude, :float
    add_column :tracks, :longitude, :float
  end
end
