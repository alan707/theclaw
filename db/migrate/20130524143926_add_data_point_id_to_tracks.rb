class AddDataPointIdToTracks < ActiveRecord::Migration
  def change
    add_column :tracks, :data_point_id, :integer
  end
end
