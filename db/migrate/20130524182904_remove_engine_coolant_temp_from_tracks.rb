class RemoveEngineCoolantTempFromTracks < ActiveRecord::Migration
  def up
    remove_column :tracks, :engine_coolant_temp
    remove_column :tracks, :total_used_fuel
    remove_column :tracks, :asset
    remove_column :tracks, :data_point_id
  end

  def down
  end
end
