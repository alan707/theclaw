class AddBigIntToTracks < ActiveRecord::Migration
  def change
    add_column :tracks, :engine_coolant_temp, :bigint
    add_column :tracks, :total_used_fuel, :bigint
    add_column :tracks, :asset, :bigint
    add_column :tracks, :data_point_id, :bigint
  end
end
