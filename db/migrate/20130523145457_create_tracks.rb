class CreateTracks < ActiveRecord::Migration
  def change
    create_table :tracks do |t|
      t.datetime :recorded_at
      t.string :asset
      t.integer :vehicle_speed
      t.integer :engine_coolant_temp
      t.integer :total_used_fuel

      t.timestamps
    end
  end
end
