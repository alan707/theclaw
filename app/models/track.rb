class Track < ActiveRecord::Base
  attr_accessible :asset, :engine_coolant_temp, :recorded_at, :total_used_fuel, :vehicle_speed, :data_point_id
  validates :data_point_id, :uniqueness => true, :presence => true
end
