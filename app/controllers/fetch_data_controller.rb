class FetchDataController < ApplicationController

require 'json'
require 'base64'
require 'net/http'



  def pull_data
    url = URI.parse('http://www.example.com/index.html')
    req = Net::HTTP::Get.new(url.to_s)
    res = Net::HTTP.start(url.host, url.port) {|http|
    http.request(req)
    }
    puts res.body
  end



# Let's imagine your web server received a POST request with this body:
request_body =<<EOF
[
  {
    "meta": {
      "account": "AccountExample",
      "event": "track"
    },
    "payload": {
      "id": 342656641079967767,
      "id_str": "342656641079967767",
      "asset": "359551XXXXX6317",
      "recorded_at": "2012-08-03T14:25:25Z",
      "received_at": "2012-08-03T14:26:28Z",
      "loc": [
        2.36687,
        48.78354
      ],
      "fields": {
        "GPS_SPEED": {
          "b64_value": "AAAAKg=="
        }
      }
    }
  }
]
EOF

events = JSON.parse(request_body)

events.each do |event|
  next if event['meta']['event'] != 'track'

  event['payload']['fields'].each do |field, data|
    if field == 'GPS_SPEED'
      b64_value = data['b64_value']
      decoded_value = Base64.decode64(b64_value).unpack('B*').first.to_i(2)
      puts "Decoded value: #{decoded_value}"
      km_h = decoded_value.to_f / 1000 * 1.852
      puts "Speed: #{km_h} / h"
    end
  end
end


end
