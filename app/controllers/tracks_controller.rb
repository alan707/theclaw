class TracksController < ApplicationController
require 'json'
require 'base64'
require 'net/http'
require 'uri'

  # GET /tracks
  # GET /tracks.json
  def index
    @tracks = Track.all


    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @tracks }
    end
  end

  # GET /tracks/1
  # GET /tracks/1.json
  def show
    @track = Track.find(params[:id])
    

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @track }
    end
  end

  def pull_data
    url = URI.parse('http://neweagleinte.integration.cloudconnect.io/api/v3/tracks')
    @req = Net::HTTP::Get.new(url.to_s)
    @req.basic_auth("9aeb6d38b108918a19c2de9ac6ce10a30c27dee3", "X")
    res = Net::HTTP.start(url.host, url.port) {|http|
    http.request(@req)
    }
    @request_body = res.body
    @result = JSON.parse(@request_body)
    
    @result.each do |event|
     @track = Track.new
      @track.data_point_id = event["id"]
      @track.asset = event["asset"]
      @track.latitude = event["location"][0]
      @track.longitude = event["location"][1]
      @track.recorded_at = event["recorded_at"]
      event["fields"].each do |field, data|
          if field == 'FMS_TOTAL_USED_FUEL'
            b64_value = data['b64_value']
            @decoded_value_fuel = Base64.decode64(b64_value).unpack('L>').first.to_i
            @track.total_used_fuel = @decoded_value_fuel
           
          end
          if field == 'FMS_ENGINE_COOLANT_TEMP'
            b64_value = data['b64_value']
            @decoded_value_temp = Base64.decode64(b64_value).unpack('L>').first.to_i
            @track.engine_coolant_temp = @decoded_value_temp
           
          end
      end
     
        @track.save
    end

    respond_to do |format|
      format.html 
      format.json { render json: @decoded_value}
    end
    
  end


  # Decodes base 64 value into ascii integers
  # 
  def decode(request_body)
  events = JSON.parse(request_body)

    events.each do |event|
      next if event['meta']['event'] != 'track'

        event['payload']['fields'].each do |field, data|
          if field == 'GPS_SPEED'
            b64_value = data['b64_value']
            decoded_value = Base64.decode64(b64_value).unpack('B*').first.to_i(2)
            puts "Decoded value: #{decoded_value}"
            km_h = decoded_value.to_f / 1000 * 1.852
            puts "Speed: #{km_h} / h"
          end
      end
    end
  end


  # GET /tracks/new
  # GET /tracks/new.json
  def new
    @tracks = Track.new
   
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @track }
    end
  end

  # GET /tracks/1/edit
  def edit
    @track = Track.find(params[:id])
  end

  # POST /tracks
  # POST /tracks.json
  def create
    @track = Track.new(params[:track])

    respond_to do |format|
      if @track.save
        format.html { redirect_to @track, notice: 'Track was successfully created.' }
        format.json { render json: @track, status: :created, location: @track }
      else
        format.html { render action: "new" }
        format.json { render json: @track.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /tracks/1
  # PUT /tracks/1.json
  def update
    @track = Track.find(params[:id])

    respond_to do |format|
      if @track.update_attributes(params[:track])
        format.html { redirect_to @track, notice: 'Track was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @track.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tracks/1
  # DELETE /tracks/1.json
  def destroy
    @track = Track.find(params[:id])
    @track.destroy

    respond_to do |format|
      format.html { redirect_to tracks_url }
      format.json { head :no_content }
    end
  end






end
